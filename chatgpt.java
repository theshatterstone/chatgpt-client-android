import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import openai.com.apikey.ApiKey;
import openai.com.chat.Chat;

public class MainActivity extends Activity {

    private EditText inputEditText;
    private TextView responseTextView;

    private Chat chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inputEditText = findViewById(R.id.inputEditText);
        responseTextView = findViewById(R.id.responseTextView);

        // Initialize ChatGPT client with your API key
        chat = new Chat(new ApiKey("YOUR_API_KEY"));
    }

    public void sendMessage(View view) {
        // Get user's input
        String input = inputEditText.getText().toString();

        // Send the input to ChatGPT and get the response
        String response = chat.sendMessage(input);

        // Display the response
        responseTextView.setText(response);
    }
}
