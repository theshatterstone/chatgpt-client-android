# Import the necessary modules
import binascii

# Prompt the user for the binary code
binary_code = input("Enter the binary code: ")

# Define the output variables
python_code = ""
file_name = "output.py"

# Convert the binary code to ASCII characters
ascii_code = binascii.unhexlify(format(int(binary_code, 2), 'x'))

# Loop through each ASCII character and build the Python code
for char in ascii_code:
  python_code += chr(char)

# Save the Python code to a file
with open(file_name, "w") as f:
  f.write(python_code)

# Print a success message
print("Python code saved to %s" % file_name)
