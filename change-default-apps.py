# Import the necessary modules
import os
import sys

# Define a function to change the default app
def change_default_app(file_type, app_name):
  # Check if the file type and app name are valid
  if not file_type or not app_name:
    print("Invalid file type or app name.")
    return

  # Set the default app for the given file type
  os.system("xdg-mime default %s %s" % (app_name, file_type))

# Get the file type and app name from the command line arguments
file_type = sys.argv[1]
app_name = sys.argv[2]

# Call the function to change the default app
change_default_app(file_type, app_name)


'''
To use this script, you would need to run it from the command line, passing in the file type and the app name as arguments. For example:

python change_default_app.py text/plain gedit

This would set the default app for text files to be gedit.
'''