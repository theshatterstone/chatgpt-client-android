# First, we need to import the `random` module to generate random numbers
import random

# Then, we need to define the main function of the game
def play_rock_paper_scissors():
  # The game needs to run indefinitely until the player decides to quit
  while True:
    # Prompt the player to enter their choice
    player_choice = input("Enter your choice (rock, paper, scissors): ")
    
    # Check if the player wants to quit the game
    if player_choice == "quit":
      break
    
    # Generate a random number between 1 and 3 to determine the computer's choice
    computer_choice = random.randint(1, 3)
    
    # Convert the computer's choice from a number to a string
    if computer_choice == 1:
      computer_choice = "rock"
    elif computer_choice == 2:
      computer_choice = "paper"
    else:
      computer_choice = "scissors"
      
    # Print the computer's choice
    print("The computer chose: " + computer_choice)
    
    # Determine the outcome of the game
    if player_choice == computer_choice:
      print("It's a tie!")
    elif player_choice == "rock" and computer_choice == "scissors":
      print("You win!")
    elif player_choice == "paper" and computer_choice == "rock":
      print("You win!")
    elif player_choice == "scissors" and computer_choice == "paper":
      print("You win!")
    else:
      print("The computer wins!")
      
# Call the main function to start the game
play_rock_paper_scissors()
