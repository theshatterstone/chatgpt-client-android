# Import the necessary modules
import os
import sys

# Define a function to install a package
def install_package(package_name):
  # Check if the package is available in the pacman repository
  if os.system("pacman -Ss %s" % package_name) == 0:
    # Install the package using pacman
    os.system("pacman -S %s" % package_name)
  else:
    # Check if the package is available in the flatpak repository
    if os.system("flatpak search %s" % package_name) == 0:
      # Install the package using flatpak
      os.system("flatpak install %s" % package_name)
    else:
      print("Package not found.")

# Get the package name from the command line arguments
package_name = sys.argv[1]

# Call the function to install the package
install_package(package_name)
